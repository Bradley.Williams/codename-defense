# Codename - Defense
# Virus Attack

![CoverImageFinal](/uploads/e4b4afbee1e6e6b4d69cad118bf6cd1d/CoverImageFinal.png)

## Concept

The idea of the project was to create a Tower Defense game. In this game you will have to prevent Virus' from making their way through your motherboard and corrupting your computer.

![TowerDefenseScreenshot2](/uploads/92825c06f81c64cd134b80e8d2a9819b/TowerDefenseScreenshot2.PNG)

## Objective:
The objective of the game is to place Towers on the board to defeat the enemies that are in coming, the final objective is to last through all 20 waves and not let a single enemy through. All towers can be upgraded once and upgrading them will increase fire rate, damage and range.

![TowerDefenseScreenshot4](/uploads/3c68e71dc6d126ab8335185537b47bd8/TowerDefenseScreenshot4.PNG)

There are 2 types of towers and enemies ground type and air type.

Ground towers can attack ground enemies and air towers can attack air enemies. However air enemies attack ground towers and ground enemies attack air towers, so setup your towers so they protect each other.

There is a time controller that allows you to speed up the game by up to 5x the speed, it also has 2x speed and just normal play and pause. While you have the time stopped you can still, buy, sell and upgrade towers.

## Controls:
WASD and Mouse - Move Camera Around

Scroll Wheel - Zoom in and Out

Left Shift - Lock Camera

Left Click - Select Tower/Place Tower/Upgrade Tower/Sell Tower

Escape - Pause/Unpause

![Wave_21](/uploads/2330feee00477c8d8d34e01965516539/Wave_21.PNG)

## Towers and their type:

Standard - Ground<br>
Jet - Air<br>
Rapid Fire - Ground<br>
Attack Helicopter - Air<br>
Healing - Can not be attacked<br>

## Enemies and their type:

Drone - Air<br>
Tank - Ground<br>


## Credits:
Picture used on game Icon: 

Icons made by Freepik from www.flaticon.com
