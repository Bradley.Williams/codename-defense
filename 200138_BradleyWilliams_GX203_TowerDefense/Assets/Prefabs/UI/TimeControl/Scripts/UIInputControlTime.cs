﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace At0m1c.TimeControl {

    public class UIInputControlTime : MonoBehaviour {

        [SerializeField] Image imagePause;
        [SerializeField] Image image1x;
        [SerializeField] Image image2x;
        [SerializeField] Image image10x;

        [SerializeField] Color activeColour;
        [SerializeField] Color inactiveColour;

        InputControlTime inputControlTime;

        void Awake () {
            inputControlTime = GetComponent<InputControlTime> ();
        }

        void OnEnable () {
            InputControlTime.OnTimeScaleChanged += TimeScaleChanged;
        }

        void OnDisable () {
            InputControlTime.OnTimeScaleChanged -= TimeScaleChanged;
        }

        public void SetTimePause () {
            inputControlTime.PauseGame ();
        }

        public void SetTime1x () {
            inputControlTime.Time1x ();
        }

        public void SetTime2x () {
            inputControlTime.Time2x ();
        }

        public void SetTime10x () {
            inputControlTime.Time10x ();
        }

        void TimeScaleChanged (float timeScale) {
            imagePause.color = inactiveColour;
            image1x.color = inactiveColour;
            image2x.color = inactiveColour;
            image10x.color = inactiveColour;

            switch (timeScale) {
                case 0:
                    imagePause.color = activeColour;
                    break;
                case 1:
                    image1x.color = activeColour;
                    break;
                case 2:
                    image2x.color = activeColour;
                    break;
                case 10:
                    image10x.color = activeColour;
                    break;
            }
        }

    }

}