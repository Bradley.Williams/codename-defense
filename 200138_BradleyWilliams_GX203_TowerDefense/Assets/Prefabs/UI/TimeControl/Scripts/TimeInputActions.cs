// GENERATED AUTOMATICALLY FROM 'Assets/Prefabs/UI/TimeControl/Scripts/TimeInputActions.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace At0m1c
{
    public class @TimeInputActions : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @TimeInputActions()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""TimeInputActions"",
    ""maps"": [
        {
            ""name"": ""TimeControls"",
            ""id"": ""85bf7765-3b9c-4ea1-a09e-82d0aaaf9f9c"",
            ""actions"": [
                {
                    ""name"": ""TimePause"",
                    ""type"": ""Button"",
                    ""id"": ""1a48ad10-6c52-4e67-9609-bea0213d5689"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""TimeScale1x"",
                    ""type"": ""Button"",
                    ""id"": ""064fa193-6acc-4fe9-a8ff-96ebed091e62"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""TimeScale2x"",
                    ""type"": ""Button"",
                    ""id"": ""b49bb858-33fc-43ff-85d4-f445c996e6c3"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""TimeScale10x"",
                    ""type"": ""Button"",
                    ""id"": ""385f2708-d42f-4882-8308-637d572ee2c1"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""06cd8a2d-9c78-44e3-b221-0af049a38f01"",
                    ""path"": ""<Keyboard>/backquote"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TimePause"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""dd425981-17f7-47e8-9fdc-db7eabe95f5c"",
                    ""path"": ""<Keyboard>/1"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TimeScale1x"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""72f8e7a8-12d2-498f-b158-749bb7f02d49"",
                    ""path"": ""<Keyboard>/2"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TimeScale2x"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b049721e-f5ae-4a7b-81f4-7be87176757a"",
                    ""path"": ""<Keyboard>/3"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""TimeScale10x"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // TimeControls
            m_TimeControls = asset.FindActionMap("TimeControls", throwIfNotFound: true);
            m_TimeControls_TimePause = m_TimeControls.FindAction("TimePause", throwIfNotFound: true);
            m_TimeControls_TimeScale1x = m_TimeControls.FindAction("TimeScale1x", throwIfNotFound: true);
            m_TimeControls_TimeScale2x = m_TimeControls.FindAction("TimeScale2x", throwIfNotFound: true);
            m_TimeControls_TimeScale10x = m_TimeControls.FindAction("TimeScale10x", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // TimeControls
        private readonly InputActionMap m_TimeControls;
        private ITimeControlsActions m_TimeControlsActionsCallbackInterface;
        private readonly InputAction m_TimeControls_TimePause;
        private readonly InputAction m_TimeControls_TimeScale1x;
        private readonly InputAction m_TimeControls_TimeScale2x;
        private readonly InputAction m_TimeControls_TimeScale10x;
        public struct TimeControlsActions
        {
            private @TimeInputActions m_Wrapper;
            public TimeControlsActions(@TimeInputActions wrapper) { m_Wrapper = wrapper; }
            public InputAction @TimePause => m_Wrapper.m_TimeControls_TimePause;
            public InputAction @TimeScale1x => m_Wrapper.m_TimeControls_TimeScale1x;
            public InputAction @TimeScale2x => m_Wrapper.m_TimeControls_TimeScale2x;
            public InputAction @TimeScale10x => m_Wrapper.m_TimeControls_TimeScale10x;
            public InputActionMap Get() { return m_Wrapper.m_TimeControls; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(TimeControlsActions set) { return set.Get(); }
            public void SetCallbacks(ITimeControlsActions instance)
            {
                if (m_Wrapper.m_TimeControlsActionsCallbackInterface != null)
                {
                    @TimePause.started -= m_Wrapper.m_TimeControlsActionsCallbackInterface.OnTimePause;
                    @TimePause.performed -= m_Wrapper.m_TimeControlsActionsCallbackInterface.OnTimePause;
                    @TimePause.canceled -= m_Wrapper.m_TimeControlsActionsCallbackInterface.OnTimePause;
                    @TimeScale1x.started -= m_Wrapper.m_TimeControlsActionsCallbackInterface.OnTimeScale1x;
                    @TimeScale1x.performed -= m_Wrapper.m_TimeControlsActionsCallbackInterface.OnTimeScale1x;
                    @TimeScale1x.canceled -= m_Wrapper.m_TimeControlsActionsCallbackInterface.OnTimeScale1x;
                    @TimeScale2x.started -= m_Wrapper.m_TimeControlsActionsCallbackInterface.OnTimeScale2x;
                    @TimeScale2x.performed -= m_Wrapper.m_TimeControlsActionsCallbackInterface.OnTimeScale2x;
                    @TimeScale2x.canceled -= m_Wrapper.m_TimeControlsActionsCallbackInterface.OnTimeScale2x;
                    @TimeScale10x.started -= m_Wrapper.m_TimeControlsActionsCallbackInterface.OnTimeScale10x;
                    @TimeScale10x.performed -= m_Wrapper.m_TimeControlsActionsCallbackInterface.OnTimeScale10x;
                    @TimeScale10x.canceled -= m_Wrapper.m_TimeControlsActionsCallbackInterface.OnTimeScale10x;
                }
                m_Wrapper.m_TimeControlsActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @TimePause.started += instance.OnTimePause;
                    @TimePause.performed += instance.OnTimePause;
                    @TimePause.canceled += instance.OnTimePause;
                    @TimeScale1x.started += instance.OnTimeScale1x;
                    @TimeScale1x.performed += instance.OnTimeScale1x;
                    @TimeScale1x.canceled += instance.OnTimeScale1x;
                    @TimeScale2x.started += instance.OnTimeScale2x;
                    @TimeScale2x.performed += instance.OnTimeScale2x;
                    @TimeScale2x.canceled += instance.OnTimeScale2x;
                    @TimeScale10x.started += instance.OnTimeScale10x;
                    @TimeScale10x.performed += instance.OnTimeScale10x;
                    @TimeScale10x.canceled += instance.OnTimeScale10x;
                }
            }
        }
        public TimeControlsActions @TimeControls => new TimeControlsActions(this);
        public interface ITimeControlsActions
        {
            void OnTimePause(InputAction.CallbackContext context);
            void OnTimeScale1x(InputAction.CallbackContext context);
            void OnTimeScale2x(InputAction.CallbackContext context);
            void OnTimeScale10x(InputAction.CallbackContext context);
        }
    }
}
