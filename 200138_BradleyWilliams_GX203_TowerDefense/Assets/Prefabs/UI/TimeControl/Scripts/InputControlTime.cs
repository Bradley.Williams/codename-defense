﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace At0m1c.TimeControl {

    public class InputControlTime : MonoBehaviour {

        public delegate void TimeEvents (float timeScale);
        public static event TimeEvents OnTimeScaleChanged;

        TimeInputActions inputActions;

        void Awake () {
            inputActions = new TimeInputActions ();

            inputActions.TimeControls.TimePause.performed += x => PauseGame ();
            inputActions.TimeControls.TimeScale1x.performed += x => Time1x ();
            inputActions.TimeControls.TimeScale2x.performed += x => Time2x ();
            inputActions.TimeControls.TimeScale10x.performed += x => Time10x ();
        }

        void OnEnable () {
            inputActions.Enable ();
        }

        void OnDisable () {
            inputActions.Disable ();
        }

        public void PauseGame () {
            Time.timeScale = 0;
            if (OnTimeScaleChanged != null) OnTimeScaleChanged (0);
            // Debug.Log (Time.timeScale);
        }

        public void Time1x () {
            Time.timeScale = 1;
            if (OnTimeScaleChanged != null) OnTimeScaleChanged (1);
            Debug.Log ($"Time set to 1x");
        }

        public void Time2x () {
            Time.timeScale = 2;
            if (OnTimeScaleChanged != null) OnTimeScaleChanged (2);
            Debug.Log ($"Time set to 2x");
        }

        public void Time10x () {
            Time.timeScale = 5;
            if (OnTimeScaleChanged != null) OnTimeScaleChanged (5);
            Debug.Log ($"Time set to 5x");
        }

    }

}