Name: Time Control
Author: Jared Brandjes
Last Update: 22/09/2020

----------
DESCRIPTION:
----------
    Pause or increase time scale from 2 inputs (keyboard or UI).

----------
REQUIREMENTS:
----------
    Unity Input System

----------
USAGE:
----------
    1. Drag TimeControl prefab into scene.
    2. Ensure there is an EventSystem is scene.

    Keyboard controls:
        - `     Pause
        - 1     1x
        - 2     2x
        - 3     10x