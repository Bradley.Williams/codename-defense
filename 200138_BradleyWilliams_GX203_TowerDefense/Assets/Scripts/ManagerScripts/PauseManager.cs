using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseManager : MonoBehaviour
{
    [HideInInspector]
    public static bool isPaused = false;
    public GameObject pauseMenu;

    private void Start() {
        pauseMenu.SetActive(false);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseCheck();
        } 
    }

    public void PauseCheck(){
        isPaused = !isPaused;
        if (isPaused)
        {
            Time.timeScale = 0;
            pauseMenu.SetActive(true);
        }
        if (!isPaused)
        {
            Time.timeScale = 1;
            pauseMenu.SetActive(false);
        }
    }
}
