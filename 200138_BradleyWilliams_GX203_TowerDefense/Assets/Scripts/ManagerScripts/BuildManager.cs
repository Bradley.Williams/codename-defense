using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildManager : MonoBehaviour
{

    public static BuildManager instance;

    public GameObject defaultTurret, rocketTurret, rapidTurret, sniperTurret;

    private TurretShopStorage turretType;
    private PlacementPiece currentPlacement;

    public UpgradeUI upgradeUI;

    private void Awake() {
        instance = this;
    }

    public bool CanBuild {get { return turretType != null; } }
    public bool HasMoney {get { return GamePlayManager.currentAmount >= turretType.turretStats.cost; } }

    public void SelectTurretToBuild (TurretShopStorage turret){
        turretType = turret;
        currentPlacement = null;

        upgradeUI.HideUI();
    }

    public void SelectPlacementPiece(PlacementPiece place){
        if (currentPlacement == place)
        {
            currentPlacement = null;
            upgradeUI.HideUI();
            return;
        }

        currentPlacement = place;
        turretType = null;

        upgradeUI.SetSelectedPiece(place);
    }

    public TurretShopStorage GetTurretType(){
        return turretType;
    }

    public void DeselectPiece(){
        currentPlacement = null;
        upgradeUI.HideUI();
        return;
    }
}
