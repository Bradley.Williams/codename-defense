using UnityEngine;
using System.Collections;
using TMPro;

public class WaveManager : MonoBehaviour
{

    public static int enemiesAlive = 0;

    public WaveStats[] waveToSpawn;

    [SerializeField] Transform spawnPoint;

    [SerializeField] float timeBetweenWaves = 5f;
    private float countDown = 2f;
    [HideInInspector]
    public int waveNum = 0;
    public TextMeshProUGUI waveNumber;

    public WinManager gameMang;

    private void Start() {
        enemiesAlive = 0;
    }

    private void Update() {

        if (enemiesAlive > 0)
        {
            return;
        }

        if (waveNum == waveToSpawn.Length)
        {
            gameMang.WinGame();
            this.enabled = false;
        }

        if (countDown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countDown = timeBetweenWaves;
            return;
        }

        countDown -= Time.deltaTime;
        countDown = Mathf.Clamp(countDown, 0f, Mathf.Infinity);
    }

    IEnumerator SpawnWave(){
        waveNumber.text = "Wave: " + (waveNum + 1).ToString();

        WaveStats wave = waveToSpawn[waveNum];

        for (var i = 0; i < wave.numToSpawn; i++)
        {
            SpawnEnemy(wave.enemyToSpawn);
            yield return new WaitForSeconds(1 / wave.rateOfSpawn);
        }
        waveNum++;
        

        Debug.Log("Wave Spawn");
    }

    void SpawnEnemy(GameObject enemyToSpawn){
        Instantiate(enemyToSpawn, spawnPoint.position, spawnPoint.rotation);
        enemiesAlive++;
    }
}
