using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class WinManager : MonoBehaviour
{
    public static bool gameIsOver;

    private void Start() {
        gameIsOver = false;
    }

    private void Update() {
        if (gameIsOver)
        {
            return;
        }
    }
    public void WinGame(){
        gameIsOver = true;
        SceneManager.LoadScene(2);
    }
}
