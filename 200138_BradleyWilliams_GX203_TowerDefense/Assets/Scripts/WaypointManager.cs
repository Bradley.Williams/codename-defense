using UnityEngine;

public class WaypointManager : MonoBehaviour
{
    /*
    This piece of code gets all the waypoints that are a child of the waypoint holder in the scene
    and puts them into an array to store all the positions of the waypoints in the scene for the 
    enemy's to follow, the object is static so that it can be accessed by other classes when needed.
    This way when an enemy is spawned in they do not need to find the waypoints they are already stored.
    */
    
    public static Transform[] waypoints;

    private void Awake() {
        waypoints = new Transform[transform.childCount];

        for (var i = 0; i < waypoints.Length; i++)
        {
            waypoints[i] = transform.GetChild(i);
        }
    }
}
