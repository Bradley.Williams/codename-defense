using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyScriptable", menuName = "Enemy", order = 1)]
public class EnemyScriptable : ScriptableObject {
    public float health,speed, fireRate;
    public int  worth, range;
}
