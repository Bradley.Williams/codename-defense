using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CurrentAmount : MonoBehaviour
{
    public TextMeshProUGUI currentAmount;
    void Update()
    {
        currentAmount.text = "$" + GamePlayManager.currentAmount.ToString();
    }
}
