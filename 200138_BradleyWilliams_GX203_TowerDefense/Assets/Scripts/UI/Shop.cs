using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public TurretShopStorage defaultTurret;
    public TurretShopStorage rocketLauncher;
    public TurretShopStorage rapidFireTurret;
    public TurretShopStorage sniperTurret;
    public TurretShopStorage healerTurret;
    BuildManager buildManager;

    private void Start() {
        buildManager = BuildManager.instance;
    }
    public void SelectStandardTurret(){
        buildManager.SelectTurretToBuild(defaultTurret);
    }

    public void SelectRocketTurret(){
        buildManager.SelectTurretToBuild(rocketLauncher);
    }

    public void SelectRapidTurret(){
        buildManager.SelectTurretToBuild(rapidFireTurret);
    }

    public void SelectSniperTurret(){
        buildManager.SelectTurretToBuild(sniperTurret);
    }

    public void SelectHealerTurret(){
        buildManager.SelectTurretToBuild(healerTurret);
    }
}
