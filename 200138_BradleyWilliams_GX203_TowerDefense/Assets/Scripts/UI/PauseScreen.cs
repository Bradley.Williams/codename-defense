using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScreen : MonoBehaviour
{
    PauseManager pauseChecker;

    private void OnEnable() {
        pauseChecker = FindObjectOfType<PauseManager>();
    }

    public void Resume(){
        pauseChecker.PauseCheck();
    }

    public void Quit(){
        Time.timeScale = 1;
        pauseChecker.PauseCheck();
        SceneManager.LoadScene(0);
    }
}
