using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeUI : MonoBehaviour
{
    public GameObject ui;
    private PlacementPiece currentPiece;
    public Text upgradeCost, sellAmount;
    public Button upgradeButton;
    public TurretShopStorage turretTypeStats;

    public void SetSelectedPiece(PlacementPiece piece){
        currentPiece = piece;

        transform.position = currentPiece.BuildPos();

        if (!currentPiece.isUpgraded)
        {
            upgradeButton.interactable = true;
            upgradeCost.text = "$" + currentPiece.turretTypeStats.turretStats.upgradeCost.ToString();
        }
        else{
            upgradeCost.text = "Max Upgrade";
            upgradeButton.interactable = false;
        }
        if (piece.isUpgraded == true)
        {
            sellAmount.text = "$" + (int) (piece.turretTypeStats.turretStats.GetSellAmount() + (currentPiece.turretTypeStats.turretStats.upgradeCost / 2));
        }
        else{
            sellAmount.text = "$" + piece.turretTypeStats.turretStats.GetSellAmount();
        }
        

        ui.SetActive(true);
    }

    public void HideUI(){
        ui.SetActive(false);
    }

    public void Upgrade(){
        currentPiece.UpgradeTurret();
        BuildManager.instance.DeselectPiece();
    }

    public void Sell(){
        currentPiece.SellTurret();
        BuildManager.instance.DeselectPiece();
    }
}
