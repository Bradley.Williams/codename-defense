using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HealerTurret : MonoBehaviour
{
    public TurretStats turretStats;
    public string turretsTag = "Turret";

    [SerializeField] float range, health;

    public ParticleSystem healingParticleSystem;
    

    public List<Turret> turretColliders =  new List<Turret>();
    [SerializeField] LayerMask turretLayer;

    private void Start() {
        range = turretStats.range;

        InvokeRepeating("Heal", 0f, 3f);
        Instantiate(healingParticleSystem, transform.position, Quaternion.identity);
    }

    void Heal(){
        turretColliders.Clear();
        //= Physics.OverlapSphere(transform.position, range, turretLayer);
        foreach (var turrets in Physics.OverlapSphere(transform.position, range, turretLayer))
        {
            turretColliders.Add(turrets.GetComponent<Turret>());
        }
        // Debug.Log(hitColliders.Length);
        foreach (var hitCollider in turretColliders)
        {
            if (hitCollider.health < hitCollider.turretStats.maxHealth)
            {
                hitCollider.health += turretStats.healingAmount;
            }

        }
        healingParticleSystem.Play();
        healingParticleSystem.Stop();

        // GameObject[] turretsToHeal = GameObject.FindGameObjectsWithTag(turretsTag);

        // foreach (GameObject turret in turretsToHeal)
        // {
        //     distanceToTurret = Vector3.Distance(transform.position, turret.transform.position);
        // }

        // if (distanceToTurret <= range)
        // {
        //     Debug.Log(turretsToHeal.Length);
        // }
    }
}
