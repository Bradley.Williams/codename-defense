using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TurretStats", menuName = "Turret", order = 0)]
public class TurretStats : ScriptableObject {

    public float maxHealth;
    public float range;
    public float fireRate;
    public float rotationSpeed;
    public GameObject bullet, turretUpgradePrefab;
    public int cost, upgradeCost;
    public int bulletDamage;
    public int healingAmount;

    public int GetSellAmount(){
        return cost / 2;
    }
}
