using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Turret : MonoBehaviour
{
    public enum turretTypes
    {
        ground,
        air,
    };

    public turretTypes thisTurretType;

    private EnemyAI.enemyTypes currentEnemyTurretType;
    private Transform currentTarget;
    public TurretStats turretStats;

    [Header("Shooting")]
    [SerializeField]public float range, health;
    public float fireRate;
    public float timeBetweenShots = 0f;

    public GameObject bullet;
    public Transform firePoint;
    public float rotationSpeed;

    public string enemyTag = "Enemy";

    public Transform turretHead;

    [Header("Health")]
    public Image healthBar;
    private bool isDead;

    // Shooting



    private void Start() {
        InvokeRepeating("CheckTarget", 0f, 0.5f);

        range = turretStats.range;
        fireRate = turretStats.fireRate;
        rotationSpeed = turretStats.rotationSpeed;
        bullet = turretStats.bullet;
        health = turretStats.maxHealth;
    }

    void CheckTarget(){
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);

        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            currentTarget = nearestEnemy.transform;
        }
        else{
            currentTarget = null;
        }
    }

    private void Update() {
        if (currentTarget == null)
        {
            return;
        }

        Vector3 directionToFace = currentTarget.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(directionToFace);
        Vector3 rotation = Quaternion.Lerp(turretHead.rotation, lookRotation, Time.deltaTime * rotationSpeed).eulerAngles;
        turretHead.rotation = Quaternion.Euler(0f, rotation.y, 0f);

        currentEnemyTurretType = currentTarget.GetComponent<EnemyAI>().enemyCurrentType;

        if (thisTurretType == turretTypes.ground && currentEnemyTurretType == EnemyAI.enemyTypes.ground)
        {
            if (timeBetweenShots <= 0f)
            {
                Shoot();
                timeBetweenShots = 1f / fireRate;
            }
        }
        if (thisTurretType == turretTypes.air && currentEnemyTurretType == EnemyAI.enemyTypes.air)
        {
            if (timeBetweenShots <= 0f)
            {
                Shoot();
                timeBetweenShots = 1f / fireRate;
            }
        }
    
        timeBetweenShots -= Time.deltaTime;
        healthBar.fillAmount = health / turretStats.maxHealth;
        if (health > turretStats.maxHealth)
        {
            health = turretStats.maxHealth;
        }
    }

    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }

    void Shoot(){
        GameObject bulletObject = Instantiate(bullet, firePoint.position, firePoint.rotation);
        Bullet bulletScript = bulletObject.GetComponent<Bullet>();

        if (bulletScript != null)
        {
            bulletScript.Seek(currentTarget);
        }
    }

    public void TakeDamage(int amount){
        health -= amount;
        // Debug.Log(enemyStats.health);
        // Debug.Log(health);

        healthBar.fillAmount = health / turretStats.maxHealth;

        if (health <= 0 && !isDead)
        {
            Die();
        }
    }

    void Die(){
        isDead = true;

        GamePlayManager.currentAmount += (turretStats.cost / 2);

        Destroy(gameObject);
    }
}
