using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TurretShopStorage
{
    public GameObject turretPrefab;
    public TurretStats turretStats;
}
