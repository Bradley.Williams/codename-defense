using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    private Transform currentTarget;
    public float speed = 70f;
    public int bulletDamage;
    public TurretStats turretStats;

    private void Start() {
        bulletDamage = turretStats.bulletDamage;
    }

    public void Seek(Transform _target){
        currentTarget = _target;
    }

    private void Update() {
        if (currentTarget == null)
        {
            Destroy(gameObject);
            return;
        }

        Vector3 directionToGo = currentTarget.position - transform.position;
        float distanceTraveled = speed * Time.deltaTime;

        if (directionToGo.magnitude <= distanceTraveled)
        {
            HitEnemy();
            return;
        }

        transform.Translate(directionToGo.normalized * distanceTraveled, Space.World);
        transform.LookAt(currentTarget);
    }

    void HitEnemy(){

        Damage(currentTarget);

        // Destroy(currentTarget.gameObject);
        Destroy(gameObject);
    }

    void Damage(Transform enemy){
        Turret e = enemy.GetComponent<Turret>();

        if (e != null)
        {
            e.TakeDamage(bulletDamage);
        }
    }
}
