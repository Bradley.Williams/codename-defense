using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyAI : MonoBehaviour
{
    public enum enemyTypes
    {
        ground,
        air
    };
    public enemyTypes enemyCurrentType;

    private Turret.turretTypes placedTurretType;
    public float health,speed;
    public int worth;
    public EnemyScriptable enemyStats;

    private Transform nextWaypoint;
    private int waypointIndex = 0;

    public Image healthBar;

    private bool isDead;
    
    [Header("Shooting")]
    // Shooting
    [SerializeField] int range;
    public float fireRate;
    public float timeBetweenShots = 0f;
    public GameObject bullet;
    public Transform firePoint;
    public string turretTag = "Turret";

    private Transform currentTarget;

    private void Start() {
        InvokeRepeating("CheckTarget", 0f, 0.5f);

        speed = enemyStats.speed;
        health = enemyStats.health;
        worth = enemyStats.worth;
        fireRate = enemyStats.fireRate;
        nextWaypoint = WaypointManager.waypoints[0];
        range = enemyStats.range;
    }

    void CheckTarget(){
        GameObject[] turrets = GameObject.FindGameObjectsWithTag(turretTag);

        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;

        foreach (GameObject turret in turrets)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, turret.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = turret;
            }
        }

        if (nearestEnemy != null && shortestDistance <= range)
        {
            currentTarget = nearestEnemy.transform;
        }
        else{
            currentTarget = null;
        }
    }

    private void Update() {
        Vector3 direction = nextWaypoint.position - transform.position;

        transform.Translate(direction.normalized * speed * Time.deltaTime, Space.World);

        WayPointCheck();

        if (currentTarget == null)
        {
            return;
        }
        //Add's Shooting to the Enemies
        placedTurretType = currentTarget.GetComponent<Turret>().thisTurretType;
        if (enemyCurrentType == enemyTypes.air && placedTurretType == Turret.turretTypes.ground)
        {
            if (timeBetweenShots <= 0f)
            {
                // Debug.Log("turret Type " + currentTurretTypes + " Enemy Type " + enemyType);
                Shoot();
                timeBetweenShots = 1f / fireRate;
            }
        }
        if (enemyCurrentType == enemyTypes.ground && placedTurretType == Turret.turretTypes.air)
        {
            if (timeBetweenShots <= 0f)
            {
                // Debug.Log("turret Type " + currentTurretTypes + " Enemy Type " + enemyType);
                Shoot();
                timeBetweenShots = 1f / fireRate;
            }
        }
        

        timeBetweenShots -= Time.deltaTime;
    }

    void Shoot(){
        GameObject bulletObject = Instantiate(bullet, firePoint.position, firePoint.rotation);
        EnemyBullet bulletScript = bulletObject.GetComponent<EnemyBullet>();

        if (bulletScript != null)
        {
            bulletScript.Seek(currentTarget);
        }
    }

    void WayPointCheck(){
        if(Vector3.Distance(transform.position, nextWaypoint.position) <= 0.2f){
            GetNextWayPoint();
        }
    }

    void GetNextWayPoint(){
        if (waypointIndex >= WaypointManager.waypoints.Length - 1)
        {
            Destroy(gameObject);
            return;
        }
        waypointIndex++;
        nextWaypoint = WaypointManager.waypoints[waypointIndex];
    }

    public void TakeDamage(int amount){
        health -= amount;
        // Debug.Log(enemyStats.health);
        // Debug.Log(health);

        healthBar.fillAmount = health / enemyStats.health;

        if (health <= 0 && !isDead)
        {
            Die();
        }
    }

    void Die(){
        isDead = true;

        GamePlayManager.currentAmount += worth;

        WaveManager.enemiesAlive--;

        Destroy(gameObject);
    }
}
