using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaveStats
{
    public GameObject enemyToSpawn;
    public int numToSpawn;
    public float rateOfSpawn;
}
