using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlacementPiece : MonoBehaviour
{
    public Color hoverColour, outOfMoney;
    public Vector3 turretPositionOffset;
    public GameObject turret;
    private Renderer rend;
    private Color startColour;

    BuildManager buildManager;

    [HideInInspector]
    public TurretShopStorage turretTypeStats;
    [HideInInspector]
    public bool isUpgraded = false;

    private void Start() {
        rend = GetComponent<Renderer>();
        startColour = rend.material.color;
        buildManager = BuildManager.instance;
    }
    private void OnMouseDown() {

        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        if (turret != null)
        {
            buildManager.SelectPlacementPiece(this);
            return;
        }

        if (!buildManager.CanBuild)
        {
            return;
        }

        BuildTurret(buildManager.GetTurretType());
    }

    void BuildTurret(TurretShopStorage turretInfo){
        if (GamePlayManager.currentAmount < turretInfo.turretStats.cost)
        {
            Debug.Log("Can not Afford this tower");
            return;
        }
        GamePlayManager.currentAmount -= turretInfo.turretStats.cost;

        GameObject _turret = (GameObject) Instantiate(turretInfo.turretPrefab, BuildPos(), Quaternion.identity);
        turret = _turret;

        turretTypeStats = turretInfo;
    }

    public void UpgradeTurret(){
        if (GamePlayManager.currentAmount < turretTypeStats.turretStats.upgradeCost)
        {
            Debug.Log("Can not Afford this tower");
            return;
        }
        GamePlayManager.currentAmount -= turretTypeStats.turretStats.upgradeCost;

        Destroy(turret);

        GameObject _turret = (GameObject) Instantiate(turretTypeStats.turretStats.turretUpgradePrefab, BuildPos(), Quaternion.identity);
        turret = _turret;

        isUpgraded = true;
    }

    public void SellTurret(){
        if (isUpgraded == true)
        {
            GamePlayManager.currentAmount += (int) ((turretTypeStats.turretStats.cost + turretTypeStats.turretStats.upgradeCost) * .5f);
            Destroy(turret);
            turretTypeStats = null;
        }
        else{
            GamePlayManager.currentAmount += (int) (turretTypeStats.turretStats.cost  * .5f);
            Destroy(turret);
            turretTypeStats = null;
        }
    }

    private void OnMouseEnter() {

        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        if (!buildManager.CanBuild)
        {
            return;
        }
        if (buildManager.HasMoney)
        {
            rend.material.color = hoverColour;
        }
        else {
            rend.material.color = outOfMoney;
        }
    }

    private void OnMouseExit() {
        rend.material.color = startColour;
    }

    public Vector3 BuildPos(){
        return transform.position + turretPositionOffset;
    }
}
